'use strict'

const _ = require('lodash')
const BPromise = require('bluebird')
const Broker = require('../../node_modules/reekoh/lib/broker.lib')

// NOTE: this class was intended to use inline with test.js
// mogoose.connect() must be called first before initializing this one

class DemoCommandRelay {
  constructor (commandRelayId, brokerUrl) {
    this.broker = new Broker()
    this.BROKER = brokerUrl
    this.commandRelayId = commandRelayId
    this.safeParse = BPromise.method(JSON.parse)
  }

  init () {
    return this.broker.connect(this.BROKER)
      .then(() => this.broker.createQueue('cmd.process'))
      .then(() => this.broker.createQueue(this.commandRelayId))
      .then(() => this.broker.queues[this.commandRelayId].consume(this.cmdProcessListener.bind(this)))
  }

  cmdProcessListener (msg) {
    if (!_.isEmpty(msg)) {
      this.safeParse(msg.content.toString() || '{}').then(content => {
        content.commandRelayId = this.commandRelayId
        return this.broker.queues['cmd.process'].publish(content)
      }).catch(console.error)
    }
    // if (msg) this.broker.channel.ack(msg)
  }
}

module.exports = DemoCommandRelay
