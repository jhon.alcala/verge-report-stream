'use strict'
/* eslint-disable new-cap */

const reekoh = require('reekoh')
const plugin = new reekoh.plugins.Stream()
const rkhLogger = new reekoh.logger('verge-report-stream')

const BPromise = require('bluebird')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
// const moment = require('moment')
const schedule = require('node-schedule')
const momentTz = require('moment-timezone')
const isEmpty = require('lodash.isempty')

function get31Dates(callback) {
  let arrDates = []
  const countDay = 31 //days
  for (let i = 0, p = Promise.resolve(); i <= countDay; i++) {
      p = p.then(_ => new Promise(resolve =>{
          arrDates.push( momentTz().tz('Australia/Melbourne').subtract((countDay-i), 'days').format('YYYY-MM-DD'))
          if(i == countDay-1) callback(arrDates)
          resolve()
  }))}
}

function jsonToCSV (fields, myData,callback) {
  const { Parser } = require('json2csv');
  
  const opts = { fields };
  
  try {
      const parser = new Parser(opts);
      const csv = parser.parse(myData);
      callback(csv);
  } catch (err) {
      console.error(err);
  }
}
function invalidReason (obj) {
  let m0 = momentTz("2019-01-08 11:55", 'Australia/Melbourne')
  let tempDate = momentTz(obj.date, 'DD/MM/YY HH:mm:ss').tz('Australia/Melbourne')
  if (tempDate.isValid()) {
    if (momentTz(tempDate).add(m0.utcOffset(), 'minutes').utc() > momentTz(obj.dateReceived)) return "Invalid Date: Future Date"
    if (momentTz(tempDate).add(m0.utcOffset(), 'minutes').utc() <= momentTz(obj.dateReceived).subtract(1,'years')) return "Invalid Date: Past Date Over 1 year"
  } else {
    return "Invalid Date Format"
  }
}
const MetadataSchema = new Schema({
  loggerId: { type: String }
})
const ReportDataSchema = new Schema({
  _id: { type: String },
  loggerId: { type: String },
  date: { type: Date },
  numOfLogs: { type: Number },
  fileName: { type: String }
})
const InvalidDataSchema = new Schema({
  _id: { type: String },
  loggerId: { type: String },
  ccid: { type: String },
  date: { type: String },
  pulses: { type: String },
  dateReceived: { type: Date }
})
const UnregisteredDataSchema = new Schema({
  _id: { type: String },
  loggerId: { type: String },
  date: { type: Date },
  registered: { type: Boolean }
})
let deviceInfo

plugin.once('ready', () => {
  const LoggersIds = mongoose.model(plugin.config.metadataTable, MetadataSchema)
  const ReportData = mongoose.model(plugin.config.reportTable, ReportDataSchema)
  const InvalidData = mongoose.model(plugin.config.invalidTable, InvalidDataSchema)
  const UnregisteredData = mongoose.model(plugin.config.unregisteredTable, UnregisteredDataSchema)

  let device = plugin.config.device || 'VERGE-REPORT-DEVICE'
  let arrSched = plugin.config.scheduled.split(',')
  let stringSchedule = `0 ${arrSched[0]} ${arrSched[1]} ${arrSched[3]} * ${arrSched[2]}`

  plugin.requestDeviceInfo(device)
    .then((resDeviceInfo) => {
      deviceInfo = resDeviceInfo
      if (isEmpty(resDeviceInfo)) {
        return plugin.logException(`Device not registered. Device ID: ${device}`)
      }
      return mongoose.connect(plugin.config.connString, { useNewUrlParser: true })
    }).catch((err) => {
      plugin.logException(err)

      setTimeout(() => {
        process.exit(1)
      }, 5000)
    })

    schedule.scheduleJob(stringSchedule, () => {
      console.log('- Schedule Arrive')
      let reportarr = []
      let arrlogs = []
      let invalidarr = []
      let unregisteredarr = []
      LoggersIds.find({}, 'loggerId')
        .then((dbResp) => {
          return BPromise.each(dbResp, (logitem)=>{
          arrlogs.push(logitem.loggerId)
          })
        })
        .then(() => {
          return Array.from(new Set(arrlogs))
        })
        .then((arrid) => {
          get31Dates(arrDates=>{
            return BPromise.each(arrid, (loggerid) => {
              let saveNumObj = {
                  log_item: `${loggerid}_Number_Of_Log`
              }
              let saveFileNameObj = {
                  log_item: `${loggerid}_FileName`
              }
              let countNumLog = 0
              let countNumFile = 0
              return BPromise.each(arrDates, (logdate)=>{
                return ReportData.find({ loggerId: loggerid, date: { $gte: momentTz(logdate).tz('Australia/Melbourne').utc(), $lt: momentTz(logdate).add(1, 'days').tz('Australia/Melbourne').utc() } })
                  .then((result) => {
                    if(!isEmpty(result)) {
                      let result_item = result[0]
                      if (result_item.numOfLogs) {
                          saveNumObj[logdate] = result_item.numOfLogs
                          countNumLog += result_item.numOfLogs
                      } else {
                          saveNumObj[logdate] = -1
                      }
                      if (result_item.fileName) {
                          saveFileNameObj[logdate] = result_item.fileName
                          if (result_item.fileName != "Not Sent Yet" && result_item.fileName != "Error Exceed Number of Log") countNumFile++
                      } else {
                          saveFileNameObj[logdate] = "No Record in Report Database"
                      }
                    }
                    else {
                      saveNumObj[logdate] = -1
                      saveFileNameObj[logdate] = "No Record in Report Database"
                    }
                      
                  })
              })
                .then(()=>{
                  saveNumObj['Expected_Total_Intervals'] = 96*32
                  saveNumObj['Total_Intervals'] = countNumLog
                  saveNumObj['Percentage'] = `${countNumLog / (96*32) * 100}%`
                  saveFileNameObj['Expected_Total_Intervals'] = 32
                  saveFileNameObj['Total_Intervals'] = countNumFile
                  saveFileNameObj['Percentage'] = `${countNumFile / 32 * 100}%`
                  reportarr.push(saveNumObj)
                  reportarr.push(saveFileNameObj)
                })      
            })
              .then(()=>{
                let m0 = momentTz("2019-01-08 11:55", 'Australia/Melbourne')
                return InvalidData.find({dateReceived: { $gte: momentTz().tz('Australia/Melbourne').subtract(31, "days").utc(), $lt: momentTz().tz('Australia/Melbourne').utc() } })
                .then((result) => {
                  result.forEach(item => {
                    item.loggerId = '_'+item.loggerId
                    item.dateReceived = momentTz(item.dateReceived).tz('Australia/Melbourne').add(m0.utcOffset(), 'minutes').utc()
                    item['invalidReason'] = invalidReason(item)
                  })
                  invalidarr = invalidarr.concat(result)
                })
              })
              .then(()=>{
                return UnregisteredData.find({date: { $gte: momentTz().tz('Australia/Melbourne').subtract(31, "days").utc(), $lt: momentTz().tz('Australia/Melbourne').utc() } })
                .then((result) => {
                  result.forEach(item => {
                    item.loggerId += '_'+item.loggerId
                  })
                  unregisteredarr = unregisteredarr.concat(result)
                })
              })
              .then(()=>{
                let csvStructure = ['log_item']
                csvStructure = csvStructure.concat(arrDates).concat(['Expected_Total_Intervals', 'Total_Intervals', 'Percentage'])
                let csvInvalidStructure = ['_id', 'loggerId', 'ccid', 'date', 'pulses', 'dateReceived', 'invalidReason']
                let csvUnregisteredStructure = ['_id', 'loggerId', 'date', 'registered']
                jsonToCSV(csvStructure, reportarr, (csv_text)=>{
                  jsonToCSV(csvInvalidStructure, invalidarr, (invalid_text)=>{
                    jsonToCSV(csvUnregisteredStructure, unregisteredarr, (unregistered_text)=>{
                      csv_text += `\r\n"Invalid Logs"\r\n${invalid_text}\r\n"Unregistered Device"\r\n${unregistered_text}`
                      let rdata = {device: device, filename: `Daily_Report_${momentTz().tz('Australia/Melbourne').format('YYYY-MM-DD')}`, csv_content: csv_text}
                      return plugin.pipe(Object.assign(rdata, { rkhDeviceInfo: deviceInfo }))
                        .then(() => {
                          return plugin.log({
                            title: 'Data Retrieved Successfully'
                          })
                        })
                    })
                  })
                })               
              })
          })
        })
    })
})
mongoose.connection.on('connected', () => {
  console.log('-Connected in Mongo')
  plugin.log('Verge Report Stream has been initialized.')
  rkhLogger.info('Verge Report Stream has been initialized.')
  // plugin.emit('init')
})

mongoose.connection.on('error', (err) => {
  plugin.logException(err)

  setTimeout(() => {
    process.exit(1)
  }, 5000)
})

module.exports = plugin
